#!/usr/bin/env python
from twisted.web.server import Site
from twisted.web.resource import Resource
from twisted.web.static import File
from twisted.internet import reactor

import json
import struct

class DiscoveryMachine(object):
	def __init__(self, server, client):
		self.server = server
		self.client = client
		self.state = self._initialize

	def __call__(self, seq, data=None):
		if self.state:
			self.state = self.state(data)

	def _initialize(self, data):
		if not data:
			return self._initialize
		self._objectAddress = data['objectAddress']
		self.server.queueCommand(self.client['ip'], {'action': 'queryU32', 'address': self._objectAddress}, self)
		print 'Object address: ' + hex(self._objectAddress)
		return self._getVtable

	def _getVtable(self, data):
		self._vtable = data['value']
		print 'Vtable address: ' + hex(self._vtable)
		self.server.queueCommand(self.client['ip'], {'action': 'querySubset', 'start': 0xE0001000, 'end': 0xE000e000}, self)
		return self._readVtable

	def _pokeVtable(self, data):
		self.server.queueCommand(self.client['ip'], {'action': 'setU32', 'address': 0xe0000000, 'value': 0xe0008000}, self)
		return self._readVtable

	def _readVtable(self, data):
		self.server.queueCommand(self.client['ip'], {'action': 'queryU32', 'address': 0xff010000}, self)
		return None

class Upload(Resource):
	def render_POST(self, request):
		memory = request.content.read()
		print hex(int(request.args['start'][0])) + ':'
		f = open('out-{:x}.bin'.format(int(request.args['start'][0])), 'w')
		f.write(memory)
		f.close()
		return ''

class CommandCenter(Resource):
	isLeaf = True
	def __init__(self):
		Resource.__init__(self)
		self.seenClients = {}

	def queueCommand(self, client, command, callback=None):
		clientInfo = self.seenClients[client]
		command['seq'] = clientInfo['seq']
		clientInfo['seq'] += 1
		clientInfo['pendingCommands'].append(command)
		if callback:
			clientInfo['callbacks'].append({'seq': command['seq'], 'callback': callback})

	def pruneSeq(self, request, body=None):
		ip = request.getClientIP()
		pendingCommands = self.seenClients[ip]['pendingCommands']
		if 'seq' in request.args:
			seq = int(request.args['seq'][0])
			pendingCommands = [command for command in pendingCommands if seq < command['seq']]
			self.seenClients[ip]['pendingCommands'] = pendingCommands
			callbacks = []
			savedCallbacks = list(self.seenClients[ip]['callbacks'])
			self.seenClients[ip]['callbacks'] = []
			for callback in savedCallbacks:
				if callback['seq'] == seq:
					callback['callback'](seq, body)
				elif callback['seq'] > seq:
					callbacks.append(callback)
			self.seenClients[ip]['callbacks'] = callbacks + self.seenClients[ip]['callbacks']
		if not pendingCommands:
			return ''
		return pendingCommands

	def render_GET(self, request):
		ip = request.getClientIP()
		if 'reset' in request.args or ip not in self.seenClients:
			self.seenClients[ip] = {
				'ip': ip,
				'pendingCommands': [],
				'callbacks': [],
				'seq': 0
			}
			request.args['seq'] = [-1]
			stateMachine = DiscoveryMachine(self, self.seenClients[ip])
			self.queueCommand(ip, {'action': 'postInfo'}, stateMachine)
		return json.dumps(self.pruneSeq(request))

	def render_POST(self, request):
		ip = request.getClientIP()
		body = request.content.read()
		command = json.loads(body)
		return json.dumps(self.pruneSeq(request, command))

root = Resource()
root.putChild('', File('index.html'))
root.putChild('upload', Upload())
root.putChild('inc', File('inc'))
root.putChild('cc', CommandCenter())
factory = Site(root)
reactor.listenTCP(8888, factory)
reactor.run()
